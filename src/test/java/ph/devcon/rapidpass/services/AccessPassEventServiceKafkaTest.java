/*
 * Copyright (c) 2020.  DevConnect Philippines, Inc.
 *  
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance 
 * with the License. You may obtain a copy of the License at
 *  
 * http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed 
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  
 * See the License for the specific language governing permissions and limitations under the License.
 */

package ph.devcon.rapidpass.services;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.connect.json.JsonSerializer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.test.context.ActiveProfiles;

import ph.devcon.rapidpass.entities.AccessPassEvent;
import ph.devcon.rapidpass.enums.AccessPassStatus;
import ph.devcon.rapidpass.enums.PassType;

/**
 * 
 * Integration testing
 * Needs a Kafka broker running on localhost:9092
 *
 */
@SpringBootTest
@ActiveProfiles("test")
@Disabled
public class AccessPassEventServiceKafkaTest {
    
	private static String TOPIC = "rapidpass-events-test1";
    private static String GROUP_ID = "rapidevent_group2";
    private static String BOOTSTRAP_SERVERS = "localhost:9092";
//    private static String TRANSACTION_ID_PREFIX = "transaction-id-prefix";
	private Consumer<String, AccessPassEvent> consumer;
	
	@Autowired
    private KafkaTemplate<String, AccessPassEvent> kafkaTemplate;
	
	@Autowired
	private AcessPassEventService accessPassEventService;
	
	final static OffsetDateTime NOW = OffsetDateTime.now();
	
    @BeforeEach
    public void setup() {
    	consumer = configureConsumer();
    }
    
	@AfterEach
    public void cleanUpEach(){
    	consumer.close();
    	consumer = null;
    }
    
    @Test
	void testRoundTrip1() {
		
    	// .eventTimestamp(NOW)
		AccessPassEvent ape1 = AccessPassEvent.builder()
    			.referenceId("ABC 123")
    			.passType(PassType.INDIVIDUAL.toString())
				.name("Jennifer Aniston")
				.status(AccessPassStatus.DECLINED.toString())
				.validFrom(NOW.plusDays(1))
				.validTo(NOW.plusDays(2))
				.eventTimestamp(NOW)
                .aporType("AG")
                .plateNumber("ABC 123")
                .accessPassID(38)
                .build();
		
//		kafkaTemplate.send(TOPIC, ape1.getReferenceId(), ape1);
		boolean isKafkaTransactionSuccessful = kafkaTemplate.executeInTransaction(kt -> {
			kt.send(TOPIC, ape1.getReferenceId(), ape1);
			return true;
		});
		assert(isKafkaTransactionSuccessful);
		
		ConsumerRecords<String, AccessPassEvent> consumerRecords = consumer.poll(Duration.ofSeconds(3));
        assertNotNull(consumerRecords);
        assertEquals(1, consumerRecords.count());

        final Iterator<ConsumerRecord<String, AccessPassEvent>> recordIterator = consumerRecords.iterator();
        while (recordIterator.hasNext()) {
            ConsumerRecord<String, AccessPassEvent> consumerRecord = recordIterator.next();
            String key = consumerRecord.key();
            AccessPassEvent ape = consumerRecord.value();
            assertNotEquals("", key);
            assertNotNull(ape);
            
            AccessPassEvent persistedApe1 = accessPassEventService.persist(ape1);
    		assertNotNull(persistedApe1);
    		assertEquals(ape1.getReferenceId(), persistedApe1.getReferenceId());
    		assertEquals(ape1.getPassType(), persistedApe1.getPassType());
    		assertEquals(ape1.getName(), persistedApe1.getName());
    		assertEquals(ape1.getStatus(), persistedApe1.getStatus());
    		assertEquals(ape1.getValidFrom(), persistedApe1.getValidFrom());
    		assertEquals(ape1.getValidTo(), persistedApe1.getValidTo());
    		assertEquals(ape1.getEventTimestamp(), persistedApe1.getEventTimestamp());
    		assertEquals(ape1.getAporType(), persistedApe1.getAporType());
    		assertEquals(ape1.getPlateNumber(), persistedApe1.getPlateNumber());
    		assertEquals(38, persistedApe1.getAccessPassID());
    		
        }
	}
	
    @Test
	void testRoundTrip2() {
		
    	// .eventTimestamp(NOW.plusSeconds(1))
    	AccessPassEvent ape2 = AccessPassEvent.builder()
    			.referenceId("GHI 789")
    			.passType(PassType.VEHICLE.toString())
				.name("Samuel Jackson")
				.status(AccessPassStatus.APPROVED.toString())
				.validFrom(NOW.plusDays(2))
				.validTo(NOW.plusDays(3))
				.eventTimestamp(NOW.plusSeconds(1))
                .aporType("BA")
                .plateNumber("GHI 789")
                .accessPassID(38)
                .build();
    	
    	// .eventTimestamp(NOW.plusDays(1))
    	AccessPassEvent ape3 = AccessPassEvent.builder()
				.referenceId("DEF 456")
				.passType(PassType.VEHICLE.toString())
				.name("Sarah Geronimo")
				.status(AccessPassStatus.DECLINED.toString())
				.validFrom(NOW.plusDays(3))
				.validTo(NOW.plusDays(4))
				.eventTimestamp(NOW.plusDays(1))
                .aporType("BP")
                .plateNumber("DEF 456")
                .accessPassID(38)
                .build();
		
    	boolean isKafkaTransactionSuccessful = kafkaTemplate.executeInTransaction(kt -> {
			kt.send(TOPIC, ape2.getReferenceId(), ape2);
			kt.send(TOPIC, ape3.getReferenceId(), ape3);
			return true;
		});
		assert(isKafkaTransactionSuccessful);
		
		ConsumerRecords<String, AccessPassEvent> consumerRecords = consumer.poll(Duration.ofSeconds(3));
        assertNotNull(consumerRecords);
        assertEquals(2, consumerRecords.count());

        final Iterator<ConsumerRecord<String, AccessPassEvent>> recordIterator = consumerRecords.iterator();
        while (recordIterator.hasNext()) {
            ConsumerRecord<String, AccessPassEvent> consumerRecord = recordIterator.next();
            String key = consumerRecord.key();
            AccessPassEvent ape = consumerRecord.value();
            assertNotEquals("", key);
            assertNotNull(ape);
            
            AccessPassEvent persistedApe2 = accessPassEventService.persist(ape2);
    		assertNotNull(persistedApe2);
    		assertEquals(ape2.getReferenceId(), persistedApe2.getReferenceId());
    		assertEquals(ape2.getPassType(), persistedApe2.getPassType());
    		assertEquals(ape2.getName(), persistedApe2.getName());
    		assertEquals(ape2.getStatus(), persistedApe2.getStatus());
    		assertEquals(ape2.getValidFrom(), persistedApe2.getValidFrom());
    		assertEquals(ape2.getValidTo(), persistedApe2.getValidTo());
    		assertEquals(ape2.getEventTimestamp(), persistedApe2.getEventTimestamp());
    		assertEquals(ape2.getAporType(), persistedApe2.getAporType());
    		assertEquals(ape2.getPlateNumber(), persistedApe2.getPlateNumber());
    		assertEquals(38, persistedApe2.getAccessPassID());
    		
    		AccessPassEvent persistedApe3 = accessPassEventService.persist(ape3);
    		assertNotNull(persistedApe3);
    		assertEquals(ape3.getReferenceId(), persistedApe3.getReferenceId());
    		assertEquals(ape3.getPassType(), persistedApe3.getPassType());
    		assertEquals(ape3.getName(), persistedApe3.getName());
    		assertEquals(ape3.getStatus(), persistedApe3.getStatus());
    		assertEquals(ape3.getValidFrom(), persistedApe3.getValidFrom());
    		assertEquals(ape3.getValidTo(), persistedApe3.getValidTo());
    		assertEquals(ape3.getEventTimestamp(), persistedApe3.getEventTimestamp());
    		assertEquals(ape3.getAporType(), persistedApe3.getAporType());
    		assertEquals(ape3.getPlateNumber(), persistedApe3.getPlateNumber());
    		assertEquals(38, persistedApe3.getAccessPassID());
        }
	}
    
	private Consumer<String, AccessPassEvent> configureConsumer() {
        Map<String, Object> consumerProps = new HashMap<>();
        consumerProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        consumerProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        consumerProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        consumerProps.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
        consumerProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        Consumer<String, AccessPassEvent> consumer = new DefaultKafkaConsumerFactory<>(
        		consumerProps, 
        		new StringDeserializer(), 
        		new JsonDeserializer<>(AccessPassEvent.class, false))
            .createConsumer();
        consumer.subscribe(Collections.singleton(TOPIC));
        consumer.poll(Duration.ZERO);
        return consumer;
    }
	
}
