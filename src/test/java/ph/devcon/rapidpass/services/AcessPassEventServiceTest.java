/*
 * Copyright (c) 2020.  DevConnect Philippines, Inc.
 *  
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance 
 * with the License. You may obtain a copy of the License at
 *  
 * http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed 
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  
 * See the License for the specific language governing permissions and limitations under the License.
 */

package ph.devcon.rapidpass.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.OffsetDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ph.devcon.rapidpass.entities.AccessPassEvent;
import ph.devcon.rapidpass.enums.AccessPassStatus;
import ph.devcon.rapidpass.enums.PassType;
import ph.devcon.rapidpass.services.controlcode.ControlCodeService;

@SpringBootTest
public class AcessPassEventServiceTest {
	
	@Autowired
	private AcessPassEventService accessPassEventService;
	
	@Autowired
	private ControlCodeService controlCodeService;
	
	final static OffsetDateTime NOW = OffsetDateTime.now();
	
	@Test
	void testAddAccessPassID() {
		
		// .eventTimestamp(NOW)
		AccessPassEvent ape1 = AccessPassEvent.builder()
    			.referenceId("ABC 123")
    			.passType(PassType.INDIVIDUAL.toString())
				.name("Jennifer Aniston")
				.status(AccessPassStatus.DECLINED.toString())
				.validFrom(NOW.plusDays(1))
				.validTo(NOW.plusDays(2))
				.eventTimestamp(NOW)
                .aporType("AG")
                .plateNumber("ABC 123")
                .controlCode(controlCodeService.encode(37))
                .build();
		
		AccessPassEvent newApe1 = accessPassEventService.addAccessPassID(ape1);
		assertNotNull(newApe1);
		assertEquals(ape1.getReferenceId(), newApe1.getReferenceId());
		assertEquals(ape1.getPassType(), newApe1.getPassType());
		assertEquals(ape1.getName(), newApe1.getName());
		assertEquals(ape1.getStatus(), newApe1.getStatus());
		assertEquals(ape1.getValidFrom(), newApe1.getValidFrom());
		assertEquals(ape1.getValidTo(), newApe1.getValidTo());
		assertEquals(ape1.getEventTimestamp(), newApe1.getEventTimestamp());
		assertEquals(ape1.getAporType(), newApe1.getAporType());
		assertEquals(ape1.getPlateNumber(), newApe1.getPlateNumber());
		assertEquals(37, newApe1.getAccessPassID());
	}
	
	@Test
    void testPersist1() {
    	
		// .eventTimestamp(NOW)
		AccessPassEvent ape1 = AccessPassEvent.builder()
    			.referenceId("ABC 123")
    			.passType(PassType.INDIVIDUAL.toString())
				.name("Jennifer Aniston")
				.status(AccessPassStatus.DECLINED.toString())
				.validFrom(NOW.plusDays(1))
				.validTo(NOW.plusDays(2))
				.eventTimestamp(NOW)
                .aporType("AG")
                .plateNumber("ABC 123")
                .controlCode(controlCodeService.encode(37))
                .build();
		
		AccessPassEvent persistedApe1 = accessPassEventService.persist(ape1);
		assertNotNull(persistedApe1);
		assertEquals(ape1.getReferenceId(), persistedApe1.getReferenceId());
		assertEquals(ape1.getPassType(), persistedApe1.getPassType());
		assertEquals(ape1.getName(), persistedApe1.getName());
		assertEquals(ape1.getStatus(), persistedApe1.getStatus());
		assertEquals(ape1.getValidFrom(), persistedApe1.getValidFrom());
		assertEquals(ape1.getValidTo(), persistedApe1.getValidTo());
		assertEquals(ape1.getEventTimestamp(), persistedApe1.getEventTimestamp());
		assertEquals(ape1.getAporType(), persistedApe1.getAporType());
		assertEquals(ape1.getPlateNumber(), persistedApe1.getPlateNumber());
		assertEquals(37, persistedApe1.getAccessPassID());
    }
	
	@Test
	void testPersist2() {
		
		// .eventTimestamp(NOW.plusSeconds(1))
    	AccessPassEvent ape2 = AccessPassEvent.builder()
    			.referenceId("GHI 789")
    			.passType(PassType.VEHICLE.toString())
				.name("Samuel Jackson")
				.status(AccessPassStatus.APPROVED.toString())
				.validFrom(NOW.plusDays(2))
				.validTo(NOW.plusDays(3))
				.eventTimestamp(NOW.plusSeconds(1))
                .aporType("BA")
                .plateNumber("GHI 789")
                .controlCode(controlCodeService.encode(37))
                .build();
    	
    	AccessPassEvent persistedApe2 = accessPassEventService.persist(ape2);
		assertNotNull(persistedApe2);
		assertEquals(ape2.getReferenceId(), persistedApe2.getReferenceId());
		assertEquals(ape2.getPassType(), persistedApe2.getPassType());
		assertEquals(ape2.getName(), persistedApe2.getName());
		assertEquals(ape2.getStatus(), persistedApe2.getStatus());
		assertEquals(ape2.getValidFrom(), persistedApe2.getValidFrom());
		assertEquals(ape2.getValidTo(), persistedApe2.getValidTo());
		assertEquals(ape2.getEventTimestamp(), persistedApe2.getEventTimestamp());
		assertEquals(ape2.getAporType(), persistedApe2.getAporType());
		assertEquals(ape2.getPlateNumber(), persistedApe2.getPlateNumber());
		assertEquals(37, persistedApe2.getAccessPassID());
	}
	
	@Test
	void testPersist3() {
		
		// .eventTimestamp(NOW.plusDays(1))
    	AccessPassEvent ape3 = AccessPassEvent.builder()
				.referenceId("DEF 456")
				.passType(PassType.VEHICLE.toString())
				.name("Sarah Geronimo")
				.status(AccessPassStatus.DECLINED.toString())
				.validFrom(NOW.plusDays(3))
				.validTo(NOW.plusDays(4))
				.eventTimestamp(NOW.plusDays(1))
                .aporType("BP")
                .plateNumber("DEF 456")
                .controlCode(controlCodeService.encode(37))
                .build();
    	
    	AccessPassEvent persistedApe3 = accessPassEventService.persist(ape3);
		assertNotNull(persistedApe3);
		assertEquals(ape3.getReferenceId(), persistedApe3.getReferenceId());
		assertEquals(ape3.getPassType(), persistedApe3.getPassType());
		assertEquals(ape3.getName(), persistedApe3.getName());
		assertEquals(ape3.getStatus(), persistedApe3.getStatus());
		assertEquals(ape3.getValidFrom(), persistedApe3.getValidFrom());
		assertEquals(ape3.getValidTo(), persistedApe3.getValidTo());
		assertEquals(ape3.getEventTimestamp(), persistedApe3.getEventTimestamp());
		assertEquals(ape3.getAporType(), persistedApe3.getAporType());
		assertEquals(ape3.getPlateNumber(), persistedApe3.getPlateNumber());
		assertEquals(37, persistedApe3.getAccessPassID());
	}
	
	@Test
	void testPersist4() {
		
		// .eventTimestamp(NOW.minusSeconds(1))
    	AccessPassEvent ape4 = AccessPassEvent.builder()
				.referenceId("JKL 321")
				.passType(PassType.INDIVIDUAL.toString())
				.name("Donald Trump")
				.status(AccessPassStatus.APPROVED.toString())
				.validFrom(NOW.plusDays(4))
				.validTo(NOW.plusDays(5))
				.eventTimestamp(NOW.minusSeconds(1))
                .aporType("CA")
                .plateNumber("JKL 321")
                .controlCode(controlCodeService.encode(37))
                .build();
    	
    	AccessPassEvent persistedApe4 = accessPassEventService.persist(ape4);
		assertNotNull(persistedApe4);
		assertEquals(ape4.getReferenceId(), persistedApe4.getReferenceId());
		assertEquals(ape4.getPassType(), persistedApe4.getPassType());
		assertEquals(ape4.getName(), persistedApe4.getName());
		assertEquals(ape4.getStatus(), persistedApe4.getStatus());
		assertEquals(ape4.getValidFrom(), persistedApe4.getValidFrom());
		assertEquals(ape4.getValidTo(), persistedApe4.getValidTo());
		assertEquals(ape4.getEventTimestamp(), persistedApe4.getEventTimestamp());
		assertEquals(ape4.getAporType(), persistedApe4.getAporType());
		assertEquals(ape4.getPlateNumber(), persistedApe4.getPlateNumber());
		assertEquals(37, persistedApe4.getAccessPassID());
	}
	
	@Test
	void testPersist5() {
		
		// .eventTimestamp(NOW.minusMinutes(1))
    	AccessPassEvent ape5 = AccessPassEvent.builder()
				.referenceId("MNO 654")
				.passType(PassType.INDIVIDUAL.toString())
				.name("James Bond")
				.status(AccessPassStatus.APPROVED.toString())
				.validFrom(NOW.plusDays(5))
				.validTo(NOW.plusDays(6))
				.eventTimestamp(NOW.minusMinutes(1))
                .aporType("CM")
                .plateNumber("MNO 654")
                .controlCode(controlCodeService.encode(37))
                .build();
    	
    	AccessPassEvent persistedApe5 = accessPassEventService.persist(ape5);
		assertNotNull(persistedApe5);
		assertEquals(ape5.getReferenceId(), persistedApe5.getReferenceId());
		assertEquals(ape5.getPassType(), persistedApe5.getPassType());
		assertEquals(ape5.getName(), persistedApe5.getName());
		assertEquals(ape5.getStatus(), persistedApe5.getStatus());
		assertEquals(ape5.getValidFrom(), persistedApe5.getValidFrom());
		assertEquals(ape5.getValidTo(), persistedApe5.getValidTo());
		assertEquals(ape5.getEventTimestamp(), persistedApe5.getEventTimestamp());
		assertEquals(ape5.getAporType(), persistedApe5.getAporType());
		assertEquals(ape5.getPlateNumber(), persistedApe5.getPlateNumber());
		assertEquals(37, persistedApe5.getAccessPassID());
	}
	
	@Test
	void testPersist6() {
		
		// .eventTimestamp(NOW.minusDays(1))
    	AccessPassEvent ape6 = AccessPassEvent.builder()
				.referenceId("PQR 987")
				.passType(PassType.INDIVIDUAL.toString())
				.name("Pops Fernandez")
				.status(AccessPassStatus.DECLINED.toString())
				.validFrom(NOW.plusDays(6))
				.validTo(NOW.plusDays(7))
				.eventTimestamp(NOW.minusDays(1))
                .aporType("DC")
                .plateNumber("PQR 987")
                .controlCode(controlCodeService.encode(37))
                .build();
    	
    	AccessPassEvent persistedApe6 = accessPassEventService.persist(ape6);
		assertNotNull(persistedApe6);
		assertEquals(ape6.getReferenceId(), persistedApe6.getReferenceId());
		assertEquals(ape6.getPassType(), persistedApe6.getPassType());
		assertEquals(ape6.getName(), persistedApe6.getName());
		assertEquals(ape6.getStatus(), persistedApe6.getStatus());
		assertEquals(ape6.getValidFrom(), persistedApe6.getValidFrom());
		assertEquals(ape6.getValidTo(), persistedApe6.getValidTo());
		assertEquals(ape6.getEventTimestamp(), persistedApe6.getEventTimestamp());
		assertEquals(ape6.getAporType(), persistedApe6.getAporType());
		assertEquals(ape6.getPlateNumber(), persistedApe6.getPlateNumber());
		assertEquals(37, persistedApe6.getAccessPassID());
	}
	
	@Test
	void testPersist7() {
		
		// .eventTimestamp(NOW.minusDays(2))
    	AccessPassEvent ape7 = AccessPassEvent.builder()
				.referenceId("STU 213")
				.passType(PassType.VEHICLE.toString())
				.name("Queen Elizabeth")
				.status(AccessPassStatus.APPROVED.toString())
				.validFrom(NOW.plusDays(3))
				.validTo(NOW.plusDays(4))
				.eventTimestamp(NOW.minusDays(2))
                .aporType("DO")
                .plateNumber("STU 213")
                .controlCode(controlCodeService.encode(37))
                .build();
    	
    	AccessPassEvent persistedApe7 = accessPassEventService.persist(ape7);
		assertNotNull(persistedApe7);
		assertEquals(ape7.getReferenceId(), persistedApe7.getReferenceId());
		assertEquals(ape7.getPassType(), persistedApe7.getPassType());
		assertEquals(ape7.getName(), persistedApe7.getName());
		assertEquals(ape7.getStatus(), persistedApe7.getStatus());
		assertEquals(ape7.getValidFrom(), persistedApe7.getValidFrom());
		assertEquals(ape7.getValidTo(), persistedApe7.getValidTo());
		assertEquals(ape7.getEventTimestamp(), persistedApe7.getEventTimestamp());
		assertEquals(ape7.getAporType(), persistedApe7.getAporType());
		assertEquals(ape7.getPlateNumber(), persistedApe7.getPlateNumber());
		assertEquals(37, persistedApe7.getAccessPassID());
	}
	
	@Test
	void testPersist8() {
		
		// .eventTimestamp(NOW.minusDays(3))
    	AccessPassEvent ape8 = AccessPassEvent.builder()
				.referenceId("VWX 546")
				.passType(PassType.VEHICLE.toString())
				.name("Ricky Martin")
				.status(AccessPassStatus.DECLINED.toString())
				.validFrom(NOW.plusDays(3))
				.validTo(NOW.plusDays(4))
				.eventTimestamp(NOW.minusDays(3))
                .aporType("DP")
                .plateNumber("VWX 546")
                .controlCode(controlCodeService.encode(37))
                .build();
    	
    	AccessPassEvent persistedApe8 = accessPassEventService.persist(ape8);
		assertNotNull(persistedApe8);
		assertEquals(ape8.getReferenceId(), persistedApe8.getReferenceId());
		assertEquals(ape8.getPassType(), persistedApe8.getPassType());
		assertEquals(ape8.getName(), persistedApe8.getName());
		assertEquals(ape8.getStatus(), persistedApe8.getStatus());
		assertEquals(ape8.getValidFrom(), persistedApe8.getValidFrom());
		assertEquals(ape8.getValidTo(), persistedApe8.getValidTo());
		assertEquals(ape8.getEventTimestamp(), persistedApe8.getEventTimestamp());
		assertEquals(ape8.getAporType(), persistedApe8.getAporType());
		assertEquals(ape8.getPlateNumber(), persistedApe8.getPlateNumber());
		assertEquals(37, persistedApe8.getAccessPassID());
	}
}
