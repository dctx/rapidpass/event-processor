/*
 * Copyright (c) 2020.  DevConnect Philippines, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */

package ph.devcon.rapidpass.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ph.devcon.rapidpass.entities.AccessPassEvent;
import ph.devcon.rapidpass.repositories.AccessPassEventRepository;
import ph.devcon.rapidpass.services.controlcode.ControlCodeService;

@Component
@Slf4j
@RequiredArgsConstructor
public class AcessPassEventService {

	@Autowired
    private final AccessPassEventRepository accessPassEventRepository;
	
	@Autowired
	private final ControlCodeService controlCodeService;

    @Transactional(isolation = Isolation.REPEATABLE_READ, transactionManager = "transactionManager")
    public AccessPassEvent persist(AccessPassEvent accessPassEvent) {
	    AccessPassEvent persistedAccessPassEvent = accessPassEventRepository.saveAndFlush(addAccessPassID(accessPassEvent));
	    if(persistedAccessPassEvent == null) {
	    	log.info("Failed to persist event to DB -> {}", persistedAccessPassEvent);
	    } else {
	    	log.info("Successfully persisted event to DB -> {}", persistedAccessPassEvent);
	    }
	    return persistedAccessPassEvent;
    }
    
    protected AccessPassEvent addAccessPassID(AccessPassEvent ape) {
    	int accessPassID = controlCodeService.decode(ape.getControlCode());
    	ape.setAccessPassID(accessPassID);
    	return ape;
    }


}