/*
 * Copyright (c) 2020.  DevConnect Philippines, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */

package ph.devcon.rapidpass.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ph.devcon.rapidpass.entities.AccessPassEvent;
import ph.devcon.rapidpass.services.AcessPassEventService;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccessPassEventConsumer {

    @Autowired
    private AcessPassEventService persistenceService;

    @Transactional
    @KafkaListener(topics = "${topic.rapidpass-events}")
    public AccessPassEvent consume(ConsumerRecord<String, AccessPassEvent> event) {
    	log.info("Event received -> {}", event.value());
    	return persistenceService.persist(event.value());
    }
}
