/*
 * Copyright (c) 2020.  DevConnect Philippines, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 */

package ph.devcon.rapidpass.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ph.devcon.rapidpass.enums.AccessPassStatus;
import ph.devcon.rapidpass.models.RapidPass;
import ph.devcon.rapidpass.models.RapidPassRequest;
import ph.devcon.rapidpass.services.PersistenceService;

@Service
@RequiredArgsConstructor
@Slf4j
public class RapidPassRequestConsumer {

//    @Value("${topic.new-requests}")
//    private String topicName;

    @Autowired
    private PersistenceService persistenceService;

    @Autowired
    ApprovalNotificationProducer notificationProducer;

    @Transactional
    @KafkaListener(topics="${topic.new-requests}")
    public void consume(ConsumerRecord<String, RapidPassRequest> request) {
        log.info("New rapidpass request -> {}}", request.value());
        // TODO Darren
        if (request != null) {
            RapidPass rapidPass = persistenceService.persist(request.value(), AccessPassStatus.APPROVED);
            if (rapidPass != null) {
                notificationProducer.sendMessage(rapidPass);
            }
        }
    }
}
